Source: deken
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-buildinfo,
 help2man,
 hy,
 python3-setuptools,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/pure-data/deken
Vcs-Git: https://salsa.debian.org/multimedia-team/pd/deken.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/pd/deken

Package: pd-deken
Architecture: all
Multi-Arch: foreign
Depends:
 puredata-gui,
 ${misc:Depends},
Recommends:
 pd-deken-apt,
 tcl-tls,
 tcllib,
Suggests:
 deken,
Description: Externals wrangler for Pure Data
 deken is a simple and easy to use package manager for Pure Data (Pd).
 .
 The deken-plugin integrates with the Pd user interface, where you can search
 for externals and install them with a single click.
 Since Pd-0.47, the deken-plugin is integrated into Pure Data (but the Debian
 package provides an updated plugin)
 .
 Available backends that provide packages are:
  - puredata.info (downloads packages via the Pure Data community website and
    installs them into the per-user installation directory (~/pd-externals))
  - apt (installs Pd packages the Debian way; packaged as "pd-deken-apt")

Package: pd-deken-apt
Architecture: all
Multi-Arch: foreign
Depends:
 puredata-gui | pd-deken,
 python3-apt,
 python3:any,
 ${misc:Depends},
Description: Externals wrangler for Pure Data (APT backend)
 deken is a simple and easy to use package manager for Pure Data (Pd).
 .
 The deken-plugin integrates with the Pd user interface, where you can search
 for externals and install them with a single click.
 .
 This addon allows one to additionally use 'apt' for installing Debian provided
 packages from within the deken-plugin (rather than downloading binaries from
 the Pure Data community website).

Package: deken
Architecture: all
Multi-Arch: foreign
Depends:
 curl,
 hy (>= 0.27),
 python3-easywebdav,
 python3-gnupg,
 python3-keyring,
 python3-macholib,
 python3-pefile,
 python3-pyelftools,
 python3-requests,
 ${misc:Depends},
Recommends:
 pd-deken,
Description: Externals wrangler for Pure Data - upload utility
 deken is a simple and easy to use package manager for Pure Data (Pd).
 .
 This package provides a command line tool that facilitates the creation
 and upload of your own packages to the official package repository
 puredata.info.
